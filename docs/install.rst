.. Licensed under the MIT Licensed

.. _install:

============
Installation
============

Install with pip
----------------

.. highlight:: console

The project is hosted on pypi : `<https://pypi.org/project/blocksim_hwil/>`_.

You can install blocksim_hwil in the usual ways. The simplest way is with pip::

    $ pip install blocksim_hwil

Checking the installation
-------------------------

If all went well, you should be able to open a command prompt, and see
blocksim_hwil installed properly:

.. parsed-literal::

    $ python -m blocksim_hwil --version
    blocksim_hwil, version |release|
    Documentation at https://blocksim_hwil.readthedocs.io/en/latest/
