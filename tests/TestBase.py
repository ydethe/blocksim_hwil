import os
import unittest

import numpy as np
from matplotlib import pyplot as plt

from blocksim.graphics import plotVerif


class TestBase(unittest.TestCase):
    def setUp(self):
        np.random.seed(1883647)

    def plotVerif(self, fig_title, *axes):
        fig = plotVerif(self.log, fig_title, *axes)

        if "SHOW_PLOT" in os.environ.keys():
            plt.show()

        return fig
