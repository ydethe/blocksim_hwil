import sys
from pathlib import Path
import time

import numpy as np
from numpy import cos, pi
import pytest

from blocksim.control.SetPoint import Step
from blocksim.control.Controller import LQRegulator, PIDController
from blocksim.control.Estimator import SteadyStateKalmanFilter
from blocksim.Simulation import Simulation
from blocksim.graphics import plotVerif

from blocksim_hwil.HWILComputer import HWILComputer

sys.path.insert(0, str(Path(__file__).parent))
from TestBase import TestBase


class TestHWIL(TestBase):
    @pytest.mark.mpl_image_compare(tolerance=5, savefig_kwargs={"dpi": 300})
    def test_hwil(self):
        # x" = -k.x - f.x' + u

        k = 40.0
        f = 5.0
        dt = 0.05

        sys = HWILComputer(port="/dev/cu.usbmodem10202501", samplingPeriod=dt)

        kal = SteadyStateKalmanFilter(
            "kal",
            dt=dt,
            shape_cmd=(1,),
            snames_state=["x", "v"],
            snames_output=["x", "v"],
        )
        kal.matA = np.array([[0, 1], [-k, -f]])
        kal.matB = np.array([[0, 1]]).T
        kal.matC = np.eye(2)
        kal.matD = np.zeros((2, 1))
        kal.matQ = np.eye(2) / 10000
        kal.matR = np.eye(2) / 100

        a = 8.0
        P = -k + 3 * a ** 2
        I = a ** 3
        D = 3 * a
        ctl = PIDController(
            "ctl", shape_estimation=(2,), snames=["u"], coeffs=(P, I, D)
        )

        stp = Step(name="stp", snames=["c"], cons=np.array([1]))

        sim = Simulation()
        sim.addComputer(sys)
        sim.addComputer(ctl)
        sim.addComputer(kal)
        sim.addComputer(stp)

        sim.connect("stp.setpoint", "ctl.setpoint")
        sim.connect("ctl.command", "teensy.command")
        sim.connect("teensy.measurement", "kal.measurement")
        sim.connect("ctl.command", "kal.command")
        sim.connect("kal.state", "ctl.estimation")

        tps = np.arange(0, 4, dt)
        sim.simulate(tps, progress_bar=True)
        log = sim.getLogger()

        axes = (
            [
                {"var": "teensy_measurement_x", "linestyle": "", "marker": "+"},
                {"var": "teensy_debug_x", "linestyle": "--"},
                {"var": "stp_setpoint_c"},
                {"var": "kal_state_x"},
            ],
        )
        fig = plotVerif(log, "test_hwil", *axes)

        return fig

    @pytest.mark.mpl_image_compare(tolerance=5, savefig_kwargs={"dpi": 300})
    def test_open_loop(self):
        dt = 0.05
        a = HWILComputer(port="/dev/cu.usbmodem10202501", samplingPeriod=dt)
        ns = 100
        omega = 6.324555320336759
        tps = np.arange(ns) * dt
        tps2 = np.empty_like(tps)
        x = np.empty(ns)
        for k in range(ns):
            otp = a.compute_outputs(0, 0, command=np.array([0.0]), measurement=None)
            x[k] = otp["measurement"][0]
            tps2[k] = time.time()

        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(tps2 - tps2[0], x, label="teensy")
        axe.plot(tps, -cos(omega * tps), label="analytic")
        axe.legend()

        return fig


if __name__ == "__main__":
    from matplotlib import pyplot as plt

    a = TestHWIL()

    a.setUp()
    a.test_hwil()
    # a.test_open_loop()

    plt.show()
