#include "Computer.h"
#include "Input.h"
#include "Output.h"

Computer::Computer(String name)
{
    m_name = name;
    m_dup_otp_name = String("");
}

void Computer::addOutput(Output otp)
{
    m_outputs.push_back(otp);
}

void Computer::defineInput(Input itp)
{
    m_input = itp;
}

void Computer::txInformation(Stream &stm)
{
    if (m_dup_otp_name != String(""))
    {
        stm.print("[ERROR]Duplicate output name in device : ");
        stm.println(m_dup_otp_name);
    }
    else
    {
        stm.println(BS_VERSION);
        stm.println(m_name);
        m_input.writeSerial(stm);
        stm.println(m_outputs.size());
        for (unsigned int it = 0; it < m_outputs.size(); ++it)
        {
            m_outputs[it].writeSerial(stm);
        }
    }
}

void Computer::rxValueForInput(Stream &stm)
{
    m_input.rxValues(stm);
}

void Computer::txValueForOutputs(Stream &stm)
{
    for (unsigned int it = 0; it < m_outputs.size(); ++it)
    {
        m_outputs[it].txValues(stm);
    }
}

int Computer::update(Stream &stm)
{
    int ret_code = 0;

    digitalWrite(LED_BUILTIN, LOW);

    if (stm.available())
    {

        char data_rcvd = stm.read(); // read one byte from serial buffer and save to data_rcvd

        if (data_rcvd == CMD_RESET)
        {
            ret_code = STAT_RESET;
        }
        else if (data_rcvd == CMD_INFO)
        {
            txInformation(stm);
            ret_code = STAT_INFO;
        }
        else if (data_rcvd == CMD_COMMAND)
        {
            rxValueForInput(stm);
            txValueForOutputs(stm);

            ret_code = STAT_COMMAND;
        }
    }

    digitalWrite(LED_BUILTIN, HIGH);

    return ret_code;
}
