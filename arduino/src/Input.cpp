#include "Input.h"
#include <stdarg.h> /* va_list, va_start, va_arg, va_end */

Input::Input()
{
    m_name = "";
}

Input::Input(String name)
{
    m_name = name;
}

void Input::addInput(String name, double *val)
{
    m_input_names.push_back(name);
    m_input_values.push_back(val);
}

void Input::writeSerial(Stream &stm)
{
    stm.println(m_name);
    stm.println(m_input_names.size());
    for (unsigned int it = 0; it < m_input_names.size(); ++it)
    {
        stm.println(m_input_names[it]);
    }
}

void Input::rxValues(Stream &stm)
{
    int n = m_input_names.size();
    int i;
    double *ptr;

    for (i = 0; i < n; i++)
    {
        String data = stm.readStringUntil('\n');
        ptr = m_input_values[i];
        *ptr = data.toFloat();
    }
}
