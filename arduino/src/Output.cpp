#include "Output.h"

Output::Output(String name)
{
    m_name = name;
}

void Output::addOutput(String name,double *val)
{
    m_output_names.push_back(name);
    m_output_values.push_back(val);
}

String &Output::getName()
{
    return m_name;
}

void Output::writeSerial(Stream &stm)
{
    stm.println(m_name);
    stm.println(m_output_names.size());
    for (unsigned int it = 0; it < m_output_names.size(); ++it)
    {
        stm.println(m_output_names[it]);
    }
}

void Output::txValues(Stream &stm)
{
    unsigned int n = m_output_names.size();
    double *ptr;

    stm.println(m_name);
    for (unsigned int i = 0; i < n; i++)
    {
        ptr = m_output_values[i];
        stm.println(*ptr);
    }
}
