#include <Arduino.h>
#include <vector>

#include "Input.h"
#include "Output.h"
#include "Computer.h"

Computer comp("teensy");

elapsedMicros sinceUpdate;
double pos_x, vel_x, new_pos, dt, cmd, n_pos_x, n_vel_x;
const double k = 40.;
const double f = 5.;
double w_0;
const double pos_x_0 = -1.;
const double vel_x_0 = 0.;
int istat;

double randomGaussian()
{
  double n = random(1000000000) / 1e9;
  return n * 2 - 1;
}

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT); // set LED pin as output
  digitalWrite(LED_BUILTIN, LOW);

  Serial.begin(115200); // initialize UART with baud rate of 115200 bps
  Serial.write("Ready\n");

  sinceUpdate = 0;

  pos_x = pos_x_0;
  vel_x = vel_x_0;
  cmd = 0.;
  w_0 = sqrt(4 * k - f * f);

  Input itp("cmd");
  itp.addInput("u", &cmd);
  comp.defineInput(itp);

  Output meas("measurement");
  meas.addOutput("x", &n_pos_x);
  meas.addOutput("v", &n_vel_x);
  comp.addOutput(meas);

  Output dbg("debug");
  dbg.addOutput("x", &pos_x);
  dbg.addOutput("v", &vel_x);
  comp.addOutput(dbg);

  istat = 0;
}

void loop()
{

  if (istat == STAT_RESET)
  {
    // reset
    sinceUpdate = 0;
    pos_x = pos_x_0;
    vel_x = vel_x_0;
    n_pos_x = pos_x + randomGaussian() * 0.1;
    n_vel_x = vel_x + randomGaussian() * 0.1;
    cmd = 0.;

  }
  else if (istat == STAT_COMMAND)
  {
    dt = float(sinceUpdate) / 1e6;
    sinceUpdate = 0;
    float St, Ct, Et;
    Ct = cos((w_0 * dt) / 2);
    St = sin((w_0 * dt) / 2);
    Et = exp(-(f * dt) / 2);
    new_pos = Et * ((St * ((f * (cmd - pos_x * k)) / k - (2 * (f * cmd - k * vel_x - pos_x * f * k)) / k)) / w_0 - (Ct * (cmd - pos_x * k)) / k) + cmd / k;
    vel_x = Et * ((Ct * ((f * (cmd - pos_x * k)) / k - (2 * (f * cmd - k * vel_x - pos_x * f * k)) / k)) / 2 + (w_0 * St * (cmd - pos_x * k)) / (2 * k)) - (f * Et * ((St * ((f * (cmd - pos_x * k)) / k - (2 * (f * cmd - k * vel_x - pos_x * f * k)) / k)) / w_0 - (Ct * (cmd - pos_x * k)) / k)) / 2;
    pos_x = new_pos;

    n_pos_x = pos_x + randomGaussian() * 0.1;
    n_vel_x = vel_x + randomGaussian() * 0.1;

  }

  istat = comp.update(Serial);

}
