#ifndef Output_h
#define Output_h

#include <Arduino.h>
#include <vector>


class Output {
protected:
    String m_name;
    std::vector<String> m_output_names;
    std::vector<double*> m_output_values;

public:
    Output(String name);
    String& getName();
    void addOutput(String name, double*);
    void writeSerial(Stream &stm);
    void txValues(Stream &stm);

};

#endif
