#ifndef Input_h
#define Input_h

#include <Arduino.h>
#include <vector>


class Input {
protected:
    String m_name;
    std::vector<String> m_input_names;
    std::vector<double*> m_input_values;

public:
    Input();
    Input(String name);
    void addInput(String name, double*);
    void writeSerial(Stream &stm);
    void rxValues(Stream &stm);

};

#endif
