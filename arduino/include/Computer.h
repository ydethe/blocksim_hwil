#ifndef Computer_h
#define Computer_h

#include <Arduino.h>
#include <vector>
#include "Input.h"
#include "Output.h"

const int BS_VERSION = 1;
const int STAT_RESET = 1;
const int STAT_INFO = 2;
const int STAT_COMMAND = 3;

#define CMD_RESET '1'
#define CMD_INFO '2'
#define CMD_COMMAND '3'

class Computer
{
protected:
    String m_name;
    Input m_input;
    std::vector<Output> m_outputs;
    String m_dup_otp_name;

public:
    Computer(String name);
    void defineInput(Input itp);
    void addOutput(Output otp);
    void txInformation(Stream &stm);
    void rxValueForInput(Stream &stm);
    void txValueForOutputs(Stream &stm);
    int update(Stream &stm);
};

#endif
