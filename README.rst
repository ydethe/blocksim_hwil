=================
blocksim_hwil
=================


.. image:: https://img.shields.io/pypi/v/blocksim_hwil.svg
        :target: https://pypi.python.org/pypi/blocksim_hwil

.. image:: https://readthedocs.org/projects/blocksim_hwil/badge/?version=latest
        :target: https://blocksim_hwil.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://gitlab.com/ydethe/blocksim_hwil/badges/master/pipeline.svg
   :target: https://gitlab.com/ydethe/blocksim_hwil/pipelines

.. image:: https://codecov.io/gl/ydethe/blocksim_hwil/branch/master/graph/badge.svg
  :target: https://codecov.io/gl/ydethe/blocksim_hwil

.. image:: https://img.shields.io/pypi/dm/blocksim_hwil
  :target: https://pypi.python.org/pypi/blocksim_hwil


A set of components to support signal in space simulations

Free software: MIT license

Development
-----------

In your virtual env::

    python setup.py develop

Features
--------

* TODO
