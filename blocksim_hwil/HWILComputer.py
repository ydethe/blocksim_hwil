import time
from typing import List

import serial
from serial.tools import list_ports
import numpy as np
from blocksim.core.Node import AComputer


RESET = b"1"
INFO = b"2"
COMMAND = b"3"


__all__ = ["HWILComputer"]


class HWILComputer(AComputer):
    """Class to interface with a USB hardware

    Args:
        port
            USB port where the device is connected. Example : "/dev/cu.usbmodem10202501"
        samplingPeriod (s)
            Sampling period of the device
        baudrate
            Baudrate of the link
        logged
            True to log the computer in the Simulation's log

    """

    __slots__ = ["__ser", "__time_last_up", "__output_shape"]

    def __init__(
        self,
        port: str,
        samplingPeriod: float,
        baudrate: int = 115200,
        logged: bool = True,
    ):
        self.__ser = serial.Serial(port=port, baudrate=baudrate, timeout=None)

        # Reset connected device
        istat = self.__ser.write(RESET)
        assert istat == 1

        # Query informations
        info = HWILComputer.getComputerInfo(port=port, baudrate=baudrate)

        name = info["name"]
        version = info["version"]
        input = info["input"]
        outputs = info["outputs"]

        super().__init__(name, logged)
        self.defineInput(name="command", shape=(len(input["list"]),), dtype=np.float64)

        for otp in outputs:
            self.defineOutput(
                name=otp["name"], snames=otp["list"], dtype=np.float64,
            )

        self.createParameter(name="version", value=version, read_only=True)
        self.createParameter(name="samplingPeriod", value=samplingPeriod)

        self.__time_last_up = time.time() - self.samplingPeriod

    @staticmethod
    def getComputerInfo(port: str, baudrate: int) -> dict:
        try:
            with serial.Serial(port=port, baudrate=baudrate, timeout=1) as ser:
                # Reset connected device
                istat = ser.write(RESET)
                assert istat == 1

                # Query informations
                istat = ser.write(INFO)
                if istat != 1:
                    return None

                buf = ser.readline().strip()
                if not buf.isdigit():
                    return None
                version = int(buf)

                buf = ser.readline().strip()
                if len(buf) == 0:
                    return None
                name = buf.decode()

                buf = ser.readline().strip()
                if len(buf) == 0:
                    return None
                inp_name = buf.decode()

                buf = ser.readline().strip()
                if not buf.isdigit():
                    return None
                input_shape = int(buf)

                in_names = []
                for k in range(input_shape):
                    buf = ser.readline().strip()
                    if len(buf) == 0:
                        return None
                    s = buf.decode()
                    in_names.append(s)

                buf = ser.readline().strip()
                if not buf.isdigit():
                    return None
                nb_otp = int(buf)

                l_otp = []
                for k in range(nb_otp):
                    buf = ser.readline().strip()
                    if len(buf) == 0:
                        return None
                    otp_name = buf.decode()

                    buf = ser.readline().strip()
                    if not buf.isdigit():
                        return None
                    output_shape = int(buf)

                    out_names = []
                    for k in range(output_shape):
                        buf = ser.readline().strip()
                        if len(buf) == 0:
                            return None
                        s = buf.decode()
                        out_names.append(s)

                    l_otp.append({"name": otp_name, "list": out_names})

                info = {
                    "name": name,
                    "version": version,
                    "input": {"name": inp_name, "list": in_names},
                    "outputs": l_otp,
                }
        except:
            info = None

        return info

    @staticmethod
    def listPorts(only_computer: bool = True) -> List[str]:
        """Lists the potentially compatible device

        Args:
            only_computer
                True to keep only the devices that return a consistent answer to :class:`HWILComputer.getComputerInfo`

        Returns:
            List of addresses such as "/dev/cu.usbmodem10202501"

        """
        params = [
            "description",
            "device",
            "hwid",
            "interface",
            "location",
            "manufacturer",
            "name",
            "pid",
            "product",
            "serial_number",
            "usb_description",
            "usb_info",
            "vid",
        ]
        print("===========================")
        print("List of available USB ports")
        print("===========================")
        for p in list_ports.comports():
            if p.product == "USB Serial":
                if not only_computer:
                    print(p.device)
                else:
                    info = HWILComputer.getComputerInfo(port=p.device, baudrate=115200)
                    print(p.device, info)
                    if not info is None:
                        print("%s - %s" % (p.device, info["name"]))

        print()

    def compute_outputs(self, t1, t2, **kwargs) -> dict:
        dt = self.__time_last_up + self.samplingPeriod - time.time()

        # Much more precise than time.sleep
        t0 = time.time_ns()
        while time.time_ns() - t0 < dt * 1e9:
            pass

        self.__time_last_up = time.time()

        itp = list(self.getListInputs())[0]
        command = kwargs[itp.getName()]

        istat = self.__ser.write(COMMAND)
        assert istat == 1

        for u in command:
            buf = str(u).encode() + b"\n"
            istat = self.__ser.write(buf)
            assert istat == len(buf)

        output = {}
        for _ in self.getListOutputs():
            buf = self.__ser.readline().strip()
            if len(buf) == 0:
                return None
            oname = buf.decode()

            otp = self.getOutputByName(oname)
            (ds,) = otp.getDataShape()
            data = np.empty(ds)
            for k in range(ds):
                buf = self.__ser.readline()
                data[k] = float(buf)

            output[oname] = data

        return output
